# OpenSim framework for Drupal

A set of tools to integrate Drupal with Open Simulator servers.
Currently (2019) under low development, but what works works.

## Current features: 

- Opensim core, featuring a preference panes to set access to Opensim database</li>
- Alternative grid info block, showing hypergrid active users as well as usual data (login URL, local accounts, active local accounts, regions).</li>

## Future features

- Show user owned regions / simulators</li>
- Link payments to activation / deactivations of regions / simulators</li>

## Useful modules</h2>

- [d4os](http://drupal.org/project/d4os): integration with Opensim, including user creatioon</li>
